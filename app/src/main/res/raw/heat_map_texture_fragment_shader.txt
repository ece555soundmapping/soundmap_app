#version 300 es
precision mediump float;
uniform float u_Value;
uniform sampler2D u_Mask;
uniform sampler2D u_Values;
in vec2 v_TexCoordinate;
layout ( location = 0 ) out vec4 outColor;
void main()
{
    outColor.r = texture(u_Values, v_TexCoordinate).r + (texture(u_Mask, v_TexCoordinate).r * u_Value);
}