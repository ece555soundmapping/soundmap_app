package edu.utk.ece555.soundmap;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.jjoe64.graphview.GraphView;

import java.io.IOException;
import java.util.Date;

public class SoundMap extends AppCompatActivity {
    private String[] mPageTitles;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavDrawer;
    private Toolbar toolbar;
    private ActionBarDrawerToggle drawerToggle;
    public static Context context;
    public ServerService serverService;
    public boolean mServerServiceBound = false;
    private HeatMap mHeatMapFragment;
    private LineGraph mLineGraphFragment;
    private MenuItem mItemSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        setContentView(R.layout.activity_sound_map);

        // Set a toolbar to replace the ActionBar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set up the Drawer List
        mPageTitles = getResources().getStringArray(R.array.page_titles_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Set up DrawerLayout events to the actionBarToggle
        drawerToggle = setupDrawerToggle();
        mDrawerLayout.setDrawerListener(drawerToggle);

        // Find drawer view
        mNavDrawer = (NavigationView) findViewById(R.id.nvView);
        MenuItem item = mNavDrawer.getMenu().getItem(0);
        item.setChecked(true);
        mItemSelected = item;
        setTitle(item.getTitle());

        // Setup drawer view
        setupDrawerContent(mNavDrawer);

        // Create Fragment
        mHeatMapFragment = new HeatMap();
        mLineGraphFragment = new LineGraph();

        // Add the first fragment
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().add(R.id.flContent, mHeatMapFragment).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Bind to Server Service
        Intent intent = new Intent(this, ServerService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Unbind from the service
        if (mServerServiceBound) {
            unbindService(mConnection);
            mServerServiceBound = false;
        }
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We're bound to Server Service
            ServerService.LocalBinder binder = (ServerService.LocalBinder) service;
            serverService = binder.getService();
            mServerServiceBound = true;
            serverService.addListener(mHeatMapFragment);
            serverService.addListener(mLineGraphFragment);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mServerServiceBound = false;
        }
    };

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                }
        );
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle saveInstanceState) {
        super.onPostCreate(saveInstanceState);

        // Sync the toggle state after onRestoreInstanceState.
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }

    public void selectDrawerItem(MenuItem menuItem) {
        DataState.DataStateInterface fragment;

       if (mItemSelected != menuItem) {
           // Create a new fragment and specify which to show
           switch (menuItem.getItemId()) {
               case R.id.nav_heat_map:
                   fragment = mHeatMapFragment;
                   mHeatMapFragment.setDataState(mLineGraphFragment.getDataState());
                   break;
               case R.id.nav_line_graph:
                   fragment = mLineGraphFragment;
                   mLineGraphFragment.setDataState(mHeatMapFragment.getDataState());
                   break;
               default:
                   fragment = mHeatMapFragment;
                   break;
           }

           // Insert the fragment by replacing any existing fragment
           FragmentManager fragmentManager = getFragmentManager();
           fragmentManager.beginTransaction().replace(R.id.flContent, (Fragment) fragment).commit();
       }

        // Highlight the selected item, update the title, and close the drawer
        menuItem.setChecked(true);
        setTitle(menuItem.getTitle());
        mDrawerLayout.closeDrawers();

        mItemSelected = menuItem;
    }
}
