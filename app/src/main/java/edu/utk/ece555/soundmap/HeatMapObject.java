package edu.utk.ece555.soundmap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.opengl.Matrix;
import android.util.Log;

import static android.opengl.GLES30.*;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

/**
 * A heat map object for use as a drawn object in OpenGL ES 3.0
 */
public class HeatMapObject {
    private final Context mActivityContext;
    private final float[] mModelMatrix;
    private final FloatBuffer mVertexBuffer;
    private final FloatBuffer mViewVertexBuffer;
    private final ShortBuffer mDrawListBuffer;
    private final FloatBuffer mTexCoordBuffer;
    private final FloatBuffer mViewTexCoordBuffer;
    private final int mProgram;
    private final int mVertexShader;
    private final int mFragmentShader;
    private int mPositionHandle;
    private int mColorHandle;
    private int mMVPMatrixHandle;
    private int mTextureUniformHandle;
    private int mTextureCoordinateHandle;
    private int mTextureDataHandle;

    private FPSCounter fpsCounter;

    // Map Coloring data
    public static final int NUM_SENSORS = 3;
    private final int mValueMapWidth = 990;
    private final int mValueMapHeight = 765;
    private final int mValueMapSize = mValueMapHeight * mValueMapWidth;
    private final int mTextureProgram;
    private final int mTextureVertexShader;
    private final int mTextureFragmentShader;

    private int valueMapFrameBuffer;
    private int[] mValueMaskDataHandles;
    private ByteBuffer mClearValueMap;
    private int mValueMapUniformHandle;
    private int mValueMapDataHandle;
    private int mContourTextureHandle;
    private int mContourUniformTextureHandle;

    // number of coordinate per vertex in this array
    static final int COORDS_PER_VERTEX = 3;
    // Coordinates for the heat map surface
    static float heatMapCoordinates[] = {
        -5.5f,   4.25f,  0.0f,   // top left
        -5.5f,  -4.25f,  0.0f,   // bottom left
         5.5f,  -4.25f,  0.0f,   // bottom right
         5.5f,   4.25f,  0.0f }; // top right

    static float viewCoordinates[] = {
        -1.0f,   1.0f,  0.0f,   // top left
        -1.0f,  -1.0f,  0.0f,   // bottom left
         1.0f,  -1.0f,  0.0f,   // bottom right
         1.0f,   1.0f,  0.0f }; // top right

    static final int mTextureCoordinateDataSize = 2;
    // Coordinates for the head map texture.
    static float heatMapTextureCoordinates[] = {
            0.0f, 0.0f,  // top left
            0.0f, 1.0f,  // bottom left
            1.0f, 1.0f,  // bottom right
            1.0f, 0.0f }; // top right
    static float viewTextureCoordinates[] = {
            0.0f, 1.0f,  // top left
            0.0f, 0.0f,  // bottom left
            1.0f, 0.0f,  // bottom right
            1.0f, 1.0f }; // top right

    private final short drawOrder[] = { 0, 1, 2, 0, 2, 3 }; // order to draw vertices
    private final int vertexStride = COORDS_PER_VERTEX * 4; // 4 bytes per vertex
    private final int texCoordStride = mTextureCoordinateDataSize * 4;
    private final int NUMBER_OF_FRAMEBUFFS = 1;
    private final int NUMBER_OF_TEXTURES = 3;
    float color[] = { 0.96f, 0.96f, 0.96f, 1.0f };

    public HeatMapObject(final Context activityContext) {
        mActivityContext = activityContext;

        // Allocate and set the model matrix
        mModelMatrix = new float[16];
        float scaleFactor = 1/5.5f;
        Matrix.setIdentityM(mModelMatrix, 0);
        Matrix.scaleM(mModelMatrix, 0, scaleFactor, scaleFactor, scaleFactor);

        // Initialize the array
        mValueMaskDataHandles = new int[NUM_SENSORS];

        // Initialize the vertex byte buffer for shape coordinates
        ByteBuffer bb = ByteBuffer.allocateDirect( heatMapCoordinates.length * 4);
        bb.order(ByteOrder.nativeOrder());
        mVertexBuffer = bb.asFloatBuffer();
        mVertexBuffer.put(heatMapCoordinates);
        mVertexBuffer.position(0);

        // Initialize the vertex byte buffer for rendering the value map
        ByteBuffer vb = ByteBuffer.allocateDirect( viewCoordinates.length * 4);
        vb.order(ByteOrder.nativeOrder());
        mViewVertexBuffer = vb.asFloatBuffer();
        mViewVertexBuffer.put(viewCoordinates);
        mViewVertexBuffer.position(0);

        // Allocate a array of zeros for use in clearing the value texture
        mClearValueMap = ByteBuffer.allocateDirect(mValueMapSize * 4);
        mClearValueMap.order(ByteOrder.nativeOrder());

        // Initialize byte buffer for draw list
        ByteBuffer dlb = ByteBuffer.allocateDirect(drawOrder.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        mDrawListBuffer = dlb.asShortBuffer();
        mDrawListBuffer.put(drawOrder);
        mDrawListBuffer.position(0);

        // Initialize the texture coordinates byte buffer.
        ByteBuffer tb = ByteBuffer.allocateDirect(heatMapTextureCoordinates.length * 4);
        tb.order(ByteOrder.nativeOrder());
        mTexCoordBuffer = tb.asFloatBuffer();
        mTexCoordBuffer.put(heatMapTextureCoordinates);
        mTexCoordBuffer.position(0);

        // Initialize the texture coordinates byte buffer.
        ByteBuffer vtb = ByteBuffer.allocateDirect(heatMapTextureCoordinates.length * 4);
        vtb.order(ByteOrder.nativeOrder());
        mViewTexCoordBuffer = vtb.asFloatBuffer();
        mViewTexCoordBuffer.put(viewTextureCoordinates);
        mViewTexCoordBuffer.position(0);

        // Prepare shaders and OpenGL program
        mVertexShader = OpenGLHelper.loadShader(GL_VERTEX_SHADER, getVertexShader());
        mFragmentShader = OpenGLHelper.loadShader(GL_FRAGMENT_SHADER, getFragmentShader());
        mProgram = glCreateProgram();
        glAttachShader(mProgram, mVertexShader);
        glAttachShader(mProgram, mFragmentShader);
        glLinkProgram(mProgram);

        // Prepare Texture Program
        mTextureVertexShader = OpenGLHelper.loadShader(GL_VERTEX_SHADER, getTextureVertexShader());
        mTextureFragmentShader = OpenGLHelper.loadShader(GL_FRAGMENT_SHADER, getTextureFragmentShader());
        mTextureProgram = glCreateProgram();
        glAttachShader(mTextureProgram, mTextureVertexShader);
        glAttachShader(mTextureProgram, mTextureFragmentShader);
        glLinkProgram(mTextureProgram);

        // Set up frame buffer
        setupFrameBuffer();

        // Load MinKao Texture
        try {
            mTextureDataHandle = OpenGLHelper.loadTexture(mActivityContext, R.mipmap.minkao_4th_high);
            OpenGLHelper.checkGlError("Load Texture");
        } catch (RuntimeException e) {
            mTextureDataHandle = OpenGLHelper.loadTexture(mActivityContext, R.mipmap.minkao_4th);
        }

        // Load Mask Textures
        loadMaskTextures();

        // Load Contour Texture
        loadContourTexture();

        // Create the FPS counter
        fpsCounter = new FPSCounter();
    }

    protected String getVertexShader()
    {
        return OpenGLHelper.readTextFileFromRawResource(mActivityContext, R.raw.heat_map_vertex_shader);
    }

    protected String getFragmentShader()
    {
        return OpenGLHelper.readTextFileFromRawResource(mActivityContext, R.raw.heat_map_fragment_shader);
    }

    protected String getTextureVertexShader()
    {
        return OpenGLHelper.readTextFileFromRawResource(mActivityContext, R.raw.heat_map_texture_vertex_shader);
    }

    protected String getTextureFragmentShader()
    {
        return OpenGLHelper.readTextFileFromRawResource(mActivityContext, R.raw.heat_map_texture_fragment_shader);
    }

    protected void setupFrameBuffer() {
        int[] tempIntArray = new int[1];

        // Generate frame buffer
        glGenFramebuffers(1, tempIntArray, 0);
        valueMapFrameBuffer = tempIntArray[0];

        // Initialize value map texture handle
        glGenTextures(1, tempIntArray, 0);
        mValueMapDataHandle = tempIntArray[0];

        // Gid to the texture in OpenGL
        glBindTexture(GL_TEXTURE_2D, mValueMapDataHandle);

        // Set filtering
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        // Set wrap mode
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        // Load the bitmap into the bound texture
        glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, mValueMapWidth, mValueMapHeight, 0, GL_RED, GL_UNSIGNED_BYTE, mClearValueMap);
    }

    protected void loadMaskTextures() {
        final float[] maskArray = new float[mValueMapSize];
        Bitmap maskBitmap;

        // Setup buffer
        ByteBuffer vb = ByteBuffer.allocateDirect(mValueMapSize * 4);
        vb.order(ByteOrder.nativeOrder());
        FloatBuffer maskBuffer = vb.asFloatBuffer();

        // Generate Texture
        glGenTextures(NUM_SENSORS, mValueMaskDataHandles, 0);

        for (int i = 0; i < NUM_SENSORS; i++) {

            switch(i) {
                case 0:
                    maskBitmap = OpenGLHelper.loadMask(mActivityContext, R.mipmap.room_406_mask);
                    break;
                case 1:
                    maskBitmap = OpenGLHelper.loadMask(mActivityContext, R.mipmap.sensor_2_mask);
                    break;
                case 2:
                    maskBitmap = OpenGLHelper.loadMask(mActivityContext, R.mipmap.sensor_3_mask);
                    break;
                default:
                    maskBitmap = OpenGLHelper.loadMask(mActivityContext, R.mipmap.room_406_mask);
                    break;
            }

            glBindTexture(GL_TEXTURE_2D, mValueMaskDataHandles[i]);

            // Set filtering
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

            // Set wrap mode
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

            // Load the bitmap mask values
            for (int row = 0; row < mValueMapHeight; row++)
                for (int col = 0; col < mValueMapWidth; col++)
                    maskArray[row * mValueMapWidth + col] = Color.alpha(maskBitmap.getPixel(col, row)) / 255.0f;

            // Store data in maskBuffer
            maskBuffer.put(maskArray);
            maskBuffer.position(0);

            // Store texture data
            glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, mValueMapWidth, mValueMapHeight, 0, GL_RED, GL_FLOAT, maskBuffer);

            maskBitmap.recycle();
        }
    }

    protected void loadContourTexture() {
        mContourTextureHandle = OpenGLHelper.loadTexture(mActivityContext, R.mipmap.heat_gradient);
//        // Define texture image
//        int[] texture8 =
//                {
//                        0x00, 0x00, 0xa0,   // Dark Blue
//                        0x00, 0x00, 0xff,   // Blue
//                        0x00, 0xa0, 0xff,   // Indigo
//                        0x00, 0xa0, 0x40,   // Dark Green
//                        0x00, 0xff, 0x00,   // Green
//                        0xff, 0xff, 0x00,   // Yellow
//                        0xff, 0xcc, 0x00,   // Orange
//                        0xff, 0x00, 0x00     // Red
//                };
//
//        ByteBuffer bb = ByteBuffer.allocateDirect(texture8.length * 2);
//        bb.order(ByteOrder.nativeOrder());
//        for (int i = 0; i < texture8.length; i++)
//        {
//            bb.put((byte)texture8[i]);
//        }
//        bb.position(0);
//
//        int[] textureIds = new int[1];
//
//        glGenTextures(1, textureIds, 0);
//        mContourTextureHandle = textureIds[0];
//        glBindTexture(GL_TEXTURE_2D, mContourTextureHandle);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
//        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
//        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 8, 1, 0, GL_RGB, GL_UNSIGNED_BYTE, bb);
    }

    protected void updateValueMap(float[] sensorValues) {
        // Use the texture program
        glUseProgram(mTextureProgram);

        // get handle to vertex shader's vPosition member
        mPositionHandle = glGetAttribLocation(mTextureProgram, "vPosition");

        // Enable a handle to the triangle vertices
        glEnableVertexAttribArray(mPositionHandle);

        // Prepare the heatMap coordinate data
        glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX, GL_FLOAT, false, vertexStride, mViewVertexBuffer);

        // Get Texture coordinate handle
        mTextureCoordinateHandle = glGetAttribLocation(mTextureProgram, "a_TexCoordinate");

        // Setup pointer to text coordinates.
        glEnableVertexAttribArray(mTextureCoordinateHandle);
        glVertexAttribPointer(mTextureCoordinateHandle, mTextureCoordinateDataSize, GL_FLOAT, false, texCoordStride, mViewTexCoordBuffer);

        // Get value handle
        final int valuesUniformHandle = glGetUniformLocation(mTextureProgram, "u_Values");

        // Set active texture unit to texture unit 1
        glActiveTexture(GL_TEXTURE1);

        // Bind the texture to this unit
        glBindTexture(GL_TEXTURE_2D, mValueMapDataHandle);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, mValueMapWidth, mValueMapHeight, 0, GL_RED, GL_UNSIGNED_BYTE, mClearValueMap);

        // Set the texture uniform
        glUniform1i(valuesUniformHandle, 1);

        // Set the viewpoint for the image
        glViewport(0, 0, mValueMapWidth, mValueMapHeight);

        // Get handle to fragment shader's u_Value member
        final int valueUniformHandle = glGetUniformLocation(mTextureProgram, "u_Value");

        // Get mask handle
        final int maskUniformHandle = glGetUniformLocation(mTextureProgram, "u_Mask");

        // Set the texture uniform
        glUniform1i(maskUniformHandle, 0);

        // For each sensor
        for (int sensor = 0; sensor < NUM_SENSORS; sensor++) {
            // Bind the frame buffer
            glBindFramebuffer(GL_FRAMEBUFFER, valueMapFrameBuffer);

            // Specify texture to the render buffer
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mValueMapDataHandle, 0);

            // Set the value for the sensor
            glUniform1f(valueUniformHandle, sensorValues[sensor]);

            // Set active texture to texture unit 0
            glActiveTexture(GL_TEXTURE0);

            // Bind the texture to this unit
            glBindTexture(GL_TEXTURE_2D, mValueMaskDataHandles[sensor]);

            // Draw the square
            glDrawElements(
                    GL_TRIANGLES, drawOrder.length,
                    GL_UNSIGNED_SHORT, mDrawListBuffer);

            // Rebind window framebuffer
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
        }

        // Disable vertex array
        glDisableVertexAttribArray(mPositionHandle);
        glDisableVertexAttribArray(mTextureCoordinateHandle);
    }

    public void draw(float[] mvpMatrix) {
        // Add program to OpenGL environment
        glUseProgram(mProgram);

        // get handle to vertex shader's vPosition member
        mPositionHandle = glGetAttribLocation(mProgram, "vPosition");

        // Enable a handle to the triangle vertices
        glEnableVertexAttribArray(mPositionHandle);

        // Prepare the heatMap coordinate data
        glVertexAttribPointer(mPositionHandle, COORDS_PER_VERTEX, GL_FLOAT, false, vertexStride, mVertexBuffer);

        // Get handle to fragment shader's vColor member
        mColorHandle = glGetUniformLocation(mProgram, "vColor");

        // Set color for drawing the heat map
        glUniform4fv(mColorHandle, 1, color, 0);

        // Get texture handle
        mTextureUniformHandle = glGetUniformLocation(mProgram, "u_Texture");

        // Set the active texture unit to texture unit 0.
        glActiveTexture(GL_TEXTURE0);

        // Bind the texture to this unit
        glBindTexture(GL_TEXTURE_2D, mTextureDataHandle);

        // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0
        glUniform1i(mTextureUniformHandle, 0);

        // Get Texture coordinate handle
        mTextureCoordinateHandle = glGetAttribLocation(mProgram, "a_TexCoordinate");

        // Setup pointer to text coordinates.
        glEnableVertexAttribArray(mTextureCoordinateHandle);
        glVertexAttribPointer(mTextureCoordinateHandle, mTextureCoordinateDataSize, GL_FLOAT, false, texCoordStride, mTexCoordBuffer);

        // Get the value map location
        mValueMapUniformHandle = glGetUniformLocation(mProgram, "u_Values");

        // Set the active texture unit to 1
        glActiveTexture(GL_TEXTURE1);

        // Bind the texture to this unit
        glBindTexture(GL_TEXTURE_2D, mValueMapDataHandle);

        // Set texture uniform to texture 1
        glUniform1i(mValueMapUniformHandle, 1);

        // Get the handle for the uniform
        mContourUniformTextureHandle = glGetUniformLocation(mProgram, "u_Contour");

        // Set the active texture unit to 2
        glActiveTexture(GL_TEXTURE2);

        // Bind the texture to this unit
        glBindTexture(GL_TEXTURE_2D, mContourTextureHandle);

        // Set contour uniform to texture 2
        glUniform1i(mContourUniformTextureHandle, 2);

        // Get the handle to shape's tranformation matrix
        mMVPMatrixHandle = glGetUniformLocation(mProgram, "uMVPMatrix");
        OpenGLHelper.checkGlError("glGetUniformLocation");

        // Apply the projection and view transformation
        Matrix.multiplyMM(mvpMatrix, 0, mvpMatrix, 0, mModelMatrix, 0);
        glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);
        OpenGLHelper.checkGlError("glUniformMatrix4fv");

        // Draw the square
        glDrawElements(
                GL_TRIANGLES, drawOrder.length,
                GL_UNSIGNED_SHORT, mDrawListBuffer);

        // Disable vertex array
        glDisableVertexAttribArray(mPositionHandle);
        glDisableVertexAttribArray(mTextureCoordinateHandle);

        fpsCounter.logFrame();
    }

    public class FPSCounter {
        long startTime = System.nanoTime();
        int frames = 0;

        public void logFrame() {
            frames++;
            if(System.nanoTime() - startTime >= 1000000000) {
                Log.d("FPSCounter", "fps: " + frames);
                frames = 0;
                startTime = System.nanoTime();
            }
        }
    }

    public void freeOpenGl()
    {
        int[] textures = new int[NUMBER_OF_TEXTURES];
        int[] frameBuffers = new int[NUMBER_OF_FRAMEBUFFS];

        glDeleteProgram(mProgram);
        glDeleteShader(mVertexShader);
        glDeleteShader(mFragmentShader);
        glDeleteProgram(mTextureProgram);
        glDeleteShader(mTextureVertexShader);
        glDeleteShader(mTextureFragmentShader);

        textures[0] = mValueMapDataHandle;
        textures[1] = mTextureDataHandle;
        textures[2] = mContourTextureHandle;
        glDeleteTextures(NUMBER_OF_TEXTURES, textures, 0);
        glDeleteTextures(NUM_SENSORS, mValueMaskDataHandles, 0);
        frameBuffers[0] = valueMapFrameBuffer;
        glDeleteFramebuffers(NUMBER_OF_FRAMEBUFFS, frameBuffers, 0);
    }
}
