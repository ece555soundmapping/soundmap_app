package edu.utk.ece555.soundmap;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLES20;
import static android.opengl.GLES30.*;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;


/**
 * Created by ayoun on 10/17/2015.
 */
public class MapGLRenderer implements GLSurfaceView.Renderer {
    public HeatMapObject heatMap;

    // mMVPMatrix is an abbreviation for "Model View Projection Matrix"
    private final float[] mMVPMatrix = new float[16];
    private final float[] mProjectionMatrix = new float[16];
    private final float[] mViewMatrix = new float[16];
    public int width;
    public int height;
    public float panX = 0.0f;
    public float panY = 0.0f;
    public float zoom = 1.0f;
    private float ratio;
    public float[] sensorValues = new float[HeatMapObject.NUM_SENSORS];

    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        // Set the background frame color
        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

        // Create the heat map object
        heatMap = new HeatMapObject(SoundMap.context);
    }

    @Override
    public void onDrawFrame(GL10 unused) {
        // Draw background color
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Set the camera position
        Matrix.setLookAtM(mViewMatrix, 0, panX, panY, 1, panX, panY, 0f, 0f, 1.0f, 0.0f);

        // Set projection matrix
        Matrix.orthoM(mProjectionMatrix, 0, -zoom, zoom, -zoom*ratio, zoom*ratio, 0.5f, 2.0f);

        // Calculate the projection and view transformation.
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);

        // Update the value map
        heatMap.updateValueMap(sensorValues);

        // Draw the Heat Map
        glViewport(0, 0, width, height);
        heatMap.draw(mMVPMatrix);
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        // Adjust the viewport based on geometry changes,
        // such as screen rotation
        this.width = width;
        this.height = height;

        ratio = (float) height/width;
    }

    public void freeOpenGl(){
        heatMap.freeOpenGl();
    }
}
