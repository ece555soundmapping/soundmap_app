package edu.utk.ece555.soundmap;

import java.util.Date;

/**
 * Created by ayoun on 11/14/2015.
 */
public class SensorData {
    protected Date mDate;
    protected float mValue;
    public static final float MAX_VALUE = 1.0f;

    public SensorData(){
        this(new Date(), 0.0f);
    }

    public SensorData(Date date, float value) {
        mDate = date;
        mValue = value;
    }

    public Date getDate(){
        return mDate;
    }

    public void setDate(Date date){
        mDate = date;
    }

    public float getValue(){
        return mValue;
    }

    public float getNormValue() {
        return Math.max(Math.min(mValue / MAX_VALUE, 1.0f), 0.0f);
    }

    public void setValue(float value){
        mValue = value;
    }

    
}
