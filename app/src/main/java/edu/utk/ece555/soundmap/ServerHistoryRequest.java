package edu.utk.ece555.soundmap;

import android.location.GpsStatus;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.jjoe64.graphview.series.DataPoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Owner on 11/14/2015.
 */
public class ServerHistoryRequest implements Runnable  {
    private static final String ADDRESS = "50.142.48.24";
    private static final int PORT = 5556;

    private Date mStart;
    private Date mEnd;

    private Thread thread;
    private Socket socket;
    private BufferedReader data_read;
    private PrintWriter data_write;

    ServerService.ServerListener mHistoryListener;

    public ServerHistoryRequest(ServerService.ServerListener listener, Date start, Date end) {
        mStart = start;
        mEnd = end;
        mHistoryListener = listener;
    }

    @Override
    public void run() {
        Log.d("ServerHistory", "In Thread");
        // Set up the server
        try {
            socket = new Socket(ADDRESS, PORT);
            data_read = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            data_write = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e)
        {
            Log.e("ServerHistory", "Failed to connect to server.", e);
            return;
        }

        data_write.println("use Sound");
        try {
            String returnV = data_read.readLine();
            Log.i("ServerHistory", "Return: " + returnV);
            if (!returnV.equals("F"))
            {
                Log.e("ServerHistory", "Failed set database.");
                return;
            }
        } catch (IOException e) {
            Log.e("ServerHistory", "Failed read from server.", e);
            return;
        }

        makeRequest();
    }

    public void makeRequest()
    {
        String line;
        int count = 0;

        final ArrayList<SensorNode> dataPoints = new ArrayList<SensorNode>();
        dataPoints.clear();
        try {
            SimpleDateFormat ft =
                    new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

            data_write.println("SELECT * FROM Measurements WHERE recordtime BETWEEN '" + ft.format(mStart) +"' AND '" +ft.format(mEnd) + "' ORDER BY nodeId");
            line = data_read.readLine();
            int nodeNumber = -1;
            if (line.equals("R")) {
                //Read First Line
                line = data_read.readLine();

                //Define Structures
                SensorNode newSensorNode;
                ArrayList<SensorData> newSensorDataList = null;

                //Read Each Line
                while (!line.equals("F") && !line.equals("E")) {
                    //Get Array of Fields
                    String[] values = line.split("[,]");

                    //If sound level is not null, add value
                    if (!values[2].equals("NULL")) {
                        //Make new structure if nodeNumber is new
                        if(Integer.parseInt(values[0])!=nodeNumber){
                            nodeNumber = Integer.parseInt(values[0]);
                            newSensorNode = new SensorNode(nodeNumber);
                            dataPoints.add(newSensorNode);
                            newSensorDataList = newSensorNode.getData();

                            Date date = ft.parse(values[1]);
                            float slevel = Float.parseFloat(values[2]);
                            newSensorDataList.add(new SensorData(date,slevel));
                        }
                        //Add to existing structure if nodeNumber has been encountered
                        else if(newSensorDataList != null){
                            Date date = ft.parse(values[1]);
                            float slevel = Float.parseFloat(values[2]);
                            newSensorDataList.add(new SensorData(date,slevel));
                        }
                    }
                    line = data_read.readLine();
                }
            }

            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    mHistoryListener.requestComplete(dataPoints);
                }
            });

        } catch (Exception e) {
            Log.e("ServerHistory", "Failed to read the data.", e);
        }
    }

    public void start() {
        thread = new Thread(this, "serverRequest");
        thread.start();
        Log.d("ServerHistory", "Start Thread");
    }
}
