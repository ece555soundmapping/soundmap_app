package edu.utk.ece555.soundmap;

import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ayoun on 10/21/2015.
 */
public class HeatMap extends Fragment implements DataState.DataStateInterface, ServerService.ServerListener, PlaybackControlFragment.PlaybackListener, DataControlFragment.OnHistoryClickListener, HistoryDialog.HistoryDialogListener{
    private MapGLSurfaceView mGLView;

    // Data for the heatmap history
    private ArrayList<SensorNode> mHistoryDataPoints = null;
    private int mPlaybackLenght;
    private DataState mDataState = new DataState();
    private boolean mDataStateSet = false;
    private DataState mPDataState = new DataState();
    private TextView mTimeText;

    //object to detect scrolling gestures
    private GestureDetectorCompat mDetector;

    //object to detect pinching gesture
    private ScaleGestureDetector mScaleDetector;
    private final float xlBound=-1.0f;
    private final float xuBound=1.0f;
    private final float ylBound=-1.0f;
    private final float yuBound=1.0f;;
    private final float lScrollBound = 0.1f;
    private final float uScrollBound = 2.0f;

    private DataControlFragment dataControl;
    private PlaybackControlFragment playbackControl;
    private SoundMap soundMap;
    private ArrayList<SensorNode> data = null;

    private Handler mTimeTextHandler = new Handler();
    private Runnable runTimeText;
    private static final int TIME_TEXT_DELAY = 2000;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the view from the layout
        View view = inflater.inflate(R.layout.fragment_heat_map, container, false);

        soundMap = (SoundMap)getActivity();

        // Set up the data control
        dataControl = new DataControlFragment();
        dataControl.setListener(this);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.data_control_fragment, dataControl).commit();

        playbackControl = new PlaybackControlFragment();
        playbackControl.setListener(this);

        // Initialize the scale detectors
        mScaleDetector = new ScaleGestureDetector(getActivity(),new ScaleListener());
        mDetector = new GestureDetectorCompat(getActivity(),new MyGestureListener());

        mTimeText = (TextView)view.findViewById(R.id.heatMapTimeText);
        runTimeText = new Runnable() {
            @Override
            public void run() {
                mTimeText.setVisibility(View.GONE);
            }
        };

        //fragment should listen for touch
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                mScaleDetector.onTouchEvent(event);
                mDetector.onTouchEvent(event);
                return true;
            }
        });

        // Get the frame that will hold the openGlView
        FrameLayout openGLView = (FrameLayout)view.findViewById(R.id.openGlView);

        // Get the content view for the OpenGl surface
        mGLView = new MapGLSurfaceView(getActivity());

        // Add the OpenGl surface to the OpenGL frame
        openGLView.addView(mGLView);

        if (mDataStateSet)
            setDataState(mPDataState);
        else
            showLive();

        // Inflate the layout for this fragment
        return view;
    }

    public DataState getDataState() {
        return mDataState;
    }

    public void setDataState(DataState state)
    {
        if (soundMap != null && soundMap.mServerServiceBound) {
            mPDataState = state;
            if (state.live)
                showLive();
            else
                showHistory(state.historyStart, state.historyEnd);
            mDataStateSet = true;
        }
    }

    private void showHistory(Date start, Date end) {
        if (playbackControl != null) {
            // Set up the data control
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.data_control_fragment, playbackControl).commit();

            mDataState.live = false;

            // Store data about the history
            mDataState.historyStart = start;
            mDataState.historyEnd = end;
            mPDataState = mDataState;
            mDataStateSet = true;

            mPlaybackLenght = (int) (end.getTime() - start.getTime());

            // Request data from the server
            soundMap.serverService.request(this, start, end);
        }
    }

    private void showLive() {
        if (dataControl != null) {
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.data_control_fragment, dataControl).commit();
            mDataState.live = true;
            mPDataState.live = true;
            mDataStateSet = true;

            if (data != null && data.size() > 0) {
                if (mDataState.live) {
                    for (SensorNode node : data) {
                        if (node.mNodeId < mGLView.mapRenderer.sensorValues.length && node.mNodeId >= 0) {
                            mGLView.mapRenderer.sensorValues[node.mNodeId] = node.mData.get(node.mData.size() - 1).getNormValue();
                        }
                    }
                }
            }
            mGLView.requestRender();
        }
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener{
        @Override
        public boolean onScale(ScaleGestureDetector detector){
            float mScaleFactor = mGLView.mapRenderer.zoom;
            float scale = detector.getScaleFactor();
            mScaleFactor *= (2.0f-detector.getScaleFactor());
            mScaleFactor = Math.max(lScrollBound, Math.min(mScaleFactor, uScrollBound));
            Log.d("Scale Listener:", String.format("%f", mScaleFactor));
            mGLView.mapRenderer.zoom = mScaleFactor;
            mGLView.requestRender();
            return true;
        }
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener{
        private static final String G_DEBUG = "Gestures";
        private float scrollx=0.0f;
        private float scrolly=0.0f;

        @Override
        public boolean onDown(MotionEvent event){
            Log.d(G_DEBUG,"onDown: "+event.toString());
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,float distanceY){
            if(e2.getPointerCount()<=1) {
                scrollx += distanceX * 2 / mGLView.mapRenderer.width * mGLView.mapRenderer.zoom;
                scrolly += distanceY * 2 / -mGLView.mapRenderer.width * mGLView.mapRenderer.zoom;

                //Check bounds and saturate if necesarry
                scrollx = Math.max(xlBound, Math.min(scrollx, xuBound));
                scrolly = Math.max(ylBound, Math.min(scrolly, yuBound));

                //set textviews
                Log.d("onScrollX: ",String.format("%f", scrollx));
                Log.d("onScrollY: ",String.format("%f", scrolly));
                mGLView.mapRenderer.panX = scrollx;
                mGLView.mapRenderer.panY = scrolly;

                mGLView.requestRender();

                //log motion
                Log.d(G_DEBUG, "On Scroll: " + Float.toString(scrollx) + " " + Float.toString(scrolly));
            }
            return true;
        }
    }

    private void showDateTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd hh:mm a", Locale.US);
        mTimeText.setText(sdf.format(date));
        mTimeText.setVisibility(View.VISIBLE);
        mTimeTextHandler.removeCallbacks(runTimeText);
        mTimeTextHandler.postDelayed(runTimeText, TIME_TEXT_DELAY);
    }

    public void updateOnSeek(int seekValue) {
        Date time;
        ArrayList<SensorData> sensorData;
        float value = 0;

        // Get the time to view
        Calendar cal = Calendar.getInstance();
        cal.setTime(mDataState.historyStart);
        cal.add(Calendar.MILLISECOND, seekValue);
        time = cal.getTime();
        showDateTime(time);


        // Find the value in the history structure and set
        for (int sensor = 0; sensor < mHistoryDataPoints.size(); sensor++)
        {
            sensorData = mHistoryDataPoints.get(sensor).mData;
            if(sensorData.size() > 0)
                value = sensorData.get(0).getNormValue();

            for (int index = 1; index < sensorData.size(); index++){
                if (sensorData.get(index).mDate.compareTo(time) > 0)
                    break;

                value = sensorData.get(index).getNormValue();
            }

            // Set the data in the sensor
            if (mGLView.mapRenderer.sensorValues.length > sensor) {
                mGLView.mapRenderer.sensorValues[sensor] = value;
            }
        }
        mGLView.requestRender();
    }

    @Override
    public void newData(int sensor, SensorData value, ArrayList<SensorNode> data) {
        this.data = data;
        if (mDataState.live) {
            if (sensor == ServerService.ALL_SENSORS) {
                for (SensorNode node : data) {
                    if (node.mNodeId < mGLView.mapRenderer.sensorValues.length && node.mNodeId >= 0) {
                        mGLView.mapRenderer.sensorValues[node.mNodeId] = node.mData.get(node.mData.size()-1).getNormValue();
                    }
                }
            }
            else {
                if (sensor < mGLView.mapRenderer.sensorValues.length && sensor >= 0) {
                    mGLView.mapRenderer.sensorValues[sensor] = value.getNormValue();
                }
            }
        }
        mGLView.requestRender();
    }

    public void requestComplete(ArrayList<SensorNode> data){
        // Handle no data case
        if (data.size() == 0) {
            Toast.makeText(getActivity(), "No History Data in Range", Toast.LENGTH_LONG).show();
            showLive();
            return;
        }

        mHistoryDataPoints = data;
        playbackControl.setPlaybackLength(mPlaybackLenght);
    }

    @Override
    public void onPause() {
        super.onPause();
        mGLView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mGLView.onResume();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mGLView.mapRenderer.freeOpenGl();
        dataControl = null;
        playbackControl = null;
        mDataStateSet = false;
    }

    public void onHistoryClick(){
        HistoryDialog historyDialog = new HistoryDialog(mPDataState.historyStart, mPDataState.historyEnd);
        historyDialog.setListener(this);
        historyDialog.show(getFragmentManager(), "historyDialog");
    }

    public void onDialogHistorySet(DialogFragment dialog, Date start, Date end) {
        dialog.dismiss();
        showHistory(start, end);
    }

    public void onDialogCancel(DialogFragment dialog) {
        dialog.dismiss();
    }

    @Override
    public void onLiveClick() {
        playbackControl.stop();
        showLive();
    }

    @Override
    public void onSeekChange(int seekValue) {
        updateOnSeek(seekValue);
    }
}
