package edu.utk.ece555.soundmap;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.widget.Toast;

import com.jjoe64.graphview.series.DataPoint;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.LogRecord;

/**
 * Created by ayoun on 11/12/2015.
 */
public class ServerService extends Service{
    public static final int ALL_SENSORS = Integer.MAX_VALUE;

    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();
    private ServerListenForNew mServerListenForNew;

    private final ArrayList<SensorNode> mCurrentSensorData = new ArrayList<SensorNode>();

    private List<ServerListener> listeners = new ArrayList<ServerListener>();

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        ServerService getService() {
            // Return this instance of LocalService so clients can call public methods
            return ServerService.this;
        }
    }

    /**
     * Interface to allow the callback of server events
     */
    public interface ServerListener {
        void newData(int sensor, SensorData value, ArrayList<SensorNode> data);
        void requestComplete(ArrayList<SensorNode> data);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Create a server communicator object
        mServerListenForNew = new ServerListenForNew(getApplication(), new ServerListenForNew.ListenForNewListener() {
            @Override
            public void onNewSensorData(int id, SensorData value) {
                ServerService.this.newData(id, value);
            }

            @Override
            public ArrayList<SensorNode> getCurrentSensorData() {
                return ServerService.this.getCurrentSensorData();
            }
        });

        // Start running the update thread
        mServerListenForNew.start();

        return mBinder;
    }

    /** Methods for clients */
    public void addListener(ServerListener listener) {
        listeners.add(listener);
        listener.newData(ALL_SENSORS, null, mCurrentSensorData);
    }

    public void removeListener(ServerListener listener) {
        listeners.remove(listener);
    }

    public void request(ServerListener listener, Date start, Date end) {
        // TODO: Handle requests
        ServerHistoryRequest historyRequest = new ServerHistoryRequest(listener, start, end);
        historyRequest.start();
    }

    public ArrayList<SensorNode> getCurrentSensorData() {
        return mCurrentSensorData;
    }

    // Broadcast the new data
    public void newData(int sensor, SensorData value){
//        if (value != null) {
//            Toast.makeText(getApplication(), "Data From " + sensor + " : " + value.mValue, Toast.LENGTH_SHORT).show();
//        }
        for (ServerListener listener : listeners) {
            listener.newData(sensor, value, mCurrentSensorData);
        }
    }

    public void runOnUiThread(Runnable run){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(run);
    }
}
