package edu.utk.ece555.soundmap;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.MotionEvent;

/**
 * Container for the OpenGL ES graphics.
 */
public class MapGLSurfaceView extends GLSurfaceView{
    public final MapGLRenderer mapRenderer;

    public MapGLSurfaceView(Context context) {
        super(context);

        // Create an OpenGL ES 3.0 context
        setEGLContextClientVersion(3);
        setEGLConfigChooser(8, 8, 8, 8, 16, 0);

        // Set the Renderer for drawing on the GLSurfaceView
        mapRenderer = new MapGLRenderer();
        setRenderer(mapRenderer);

        // Render only when there is a change in the drawing data
        setRenderMode(RENDERMODE_WHEN_DIRTY);
    }
}
