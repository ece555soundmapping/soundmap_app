package edu.utk.ece555.soundmap;

import java.util.Date;

/**
 * Created by ayoun on 11/15/2015.
 */
public class DataState {
    public interface DataStateInterface {
        public DataState getDataState();
        public void setDataState(DataState state);
    }
    public Date historyStart = new Date();
    public Date historyEnd = new Date();
    public boolean live = false;
}
