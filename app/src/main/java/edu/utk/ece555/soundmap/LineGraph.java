package edu.utk.ece555.soundmap;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LabelFormatter;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class LineGraph extends Fragment implements DataState.DataStateInterface, ServerService.ServerListener, DataControlFragment.OnLiveClickListener, DataControlFragment.OnHistoryClickListener, HistoryDialog.HistoryDialogListener
{
    private GraphView graphView;
    private DataControlFragment dataControl;
    private SoundMap soundMap;
    private DataState mDataState = new DataState();
    private DataState mPDataState = new DataState();
    private ArrayList<SensorNode> data = null;
    private boolean mLiveVisibility = false;
    private boolean mDataStateSet = false;
    private SeriesData mSeriesData = new SeriesData();

    private class SeriesData {
        public ArrayList<SeriesPoint> seriesList;

        public class SeriesPoint {
            public int id;
            public LineGraphSeries<DataPoint> series;
        }

        public SeriesData() {
            seriesList = new ArrayList<SeriesPoint>();
        }

        public void clearSeries() {
            seriesList.clear();
        }

        public LineGraphSeries<DataPoint> getSeriesForId(int id, GraphView graph) {
            for(SeriesPoint data : seriesList) {
                if (data.id == id)
                    return data.series;
            }

            SeriesPoint point = new SeriesPoint();
            point.id = id;
            point.series = new LineGraphSeries<DataPoint>();
            seriesList.add(point);

            switch (id) {
                case 0:
                    point.series.setColor(Color.BLUE);
                    point.series.setTitle("Room: 406");
                    break;
                case 1:
                    point.series.setColor(Color.RED);
                    point.series.setTitle("Room: 217");
                    break;
                case 2:
                    point.series.setColor(Color.GREEN);
                    point.series.setTitle("Room: 416");
                    break;
                default:
                    point.series.setColor(Color.BLACK);
                    point.series.setTitle("Node: " + id);
                    break;
            }

            graph.addSeries(point.series);
            return point.series;
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view;

        // Get the sound map activity
        soundMap = (SoundMap)getActivity();

        mSeriesData.clearSeries();

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_line_graph, container, false);

        graphView = (GraphView) view.findViewById(R.id.soundGraph);

        graphView.getGridLabelRenderer().setLabelFormatter(new LabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX)
                {
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd h:mm a", Locale.US);
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis((long) value);
                    return sdf.format(cal.getTime());
                }
                return null;
            }

            @Override
            public void setViewport(Viewport viewport) {

            }
        });
        graphView.getGridLabelRenderer().setNumHorizontalLabels(3);

        dataControl = new DataControlFragment();
        dataControl.setListener(this);
        dataControl.setLiveVisibility(mLiveVisibility);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.data_control_fragment, dataControl).commit();

        graphView.setTitle(getString(R.string.graph_title));
        graphView.getViewport().setMinY(0.0);
        graphView.getViewport().setMaxY(SensorData.MAX_VALUE);
        graphView.getViewport().setYAxisBoundsManual(true);
        graphView.getViewport().setXAxisBoundsManual(true);
        graphView.getLegendRenderer().setVisible(true);
        graphView.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
        graphView.getLegendRenderer().setWidth(370);

        if (mDataStateSet)
            setDataState(mPDataState);
        else
            showLive();

        return view;
    }

    public DataState getDataState() {
        return mDataState;
    }

    public void setDataState(DataState state) {
        if (soundMap != null && soundMap.mServerServiceBound) {
            mPDataState = state;
            if (state.live)
                showLive();
            else
                showHistory(state.historyStart, state.historyEnd);
            mDataStateSet = true;
        }
    }

    private void showHistory(Date start, Date end) {
        if (dataControl != null) {
            dataControl.setLiveVisibility(true);

            mLiveVisibility = true;
            mDataState.live = false;
            mDataState.historyStart = start;
            mDataState.historyEnd = end;
            mPDataState = mDataState;
            mDataStateSet = true;

            // Request the data from the server
            soundMap.serverService.request(this, start, end);
        }
    }

    private void showLive() {
        if (dataControl != null) {
            dataControl.setLiveVisibility(false);
            mLiveVisibility = false;
            mDataState.live = true;
            mPDataState.live = true;
            mDataStateSet = true;
            if (data != null && data.size() > 0)
                requestComplete(data);
        }
    }

    public void newData(int sensor, SensorData value, ArrayList<SensorNode> data) {
        this.data = data;
        if (mDataState.live && data != null && data.size() > 0) {
            requestComplete(data);
        }
    }

    public void requestComplete(ArrayList<SensorNode> data){
        try {
            if (graphView != null) {
                double minX = Float.MAX_VALUE;
                double maxX = Float.MIN_VALUE;
                for (SensorNode node : data) {
                    LineGraphSeries<DataPoint> series = mSeriesData.getSeriesForId(node.getNodeId(), graphView);
                    DataPoint[] pointArray = new DataPoint[node.mData.size()];
                    int index = 0;
                    for (SensorData nodeData : node.mData) {
                        pointArray[index] = new DataPoint(nodeData.getDate(), nodeData.getValue());
                        if (pointArray[index].getX() > maxX) maxX = pointArray[index].getX();
                        if (pointArray[index].getX() < minX) minX = pointArray[index].getX();
                        index++;
                    }
                    // TODO: Find first and last better
                    series.resetData(pointArray);
                }

                if (minX == Float.MAX_VALUE || maxX == Float.MIN_VALUE || minX == maxX) {
                    Toast.makeText(getActivity(), "No History Data in Range", Toast.LENGTH_LONG).show();
                    showLive();
                } else {
                    graphView.getViewport().setMinX(minX);
                    graphView.getViewport().setMaxX(maxX);
                }

            }
        } catch (Exception e) {}
    }

    public void onHistoryClick(){
        HistoryDialog historyDialog = new HistoryDialog(mPDataState.historyStart, mPDataState.historyEnd);
        historyDialog.setListener(this);
        historyDialog.show(getFragmentManager(), "historyDialog");
    }

    public void onLiveClick() {
        showLive();
    }

    public void onDialogHistorySet(DialogFragment dialog, Date start, Date end) {
        dialog.dismiss();
        showHistory(start, end);
    }

    public void onDialogCancel(DialogFragment dialog) {
        dialog.dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDataStateSet = false;
    }
}
