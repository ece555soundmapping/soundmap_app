package edu.utk.ece555.soundmap;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import java.util.Date;

/**
 * Created by ayoun on 11/14/2015.
 */
public class HistoryDialog extends DialogFragment {
    HistoryDialogListener mListener;
    Button mStartTimeButton;
    Button mEndTimeButton;
    Button mCancelButton;
    Button mSetButton;
    DateTimeDialogFragment mStartDateTimeDialog;
    DateTimeDialogFragment mEndDateTimeDialog;
    Date mStartDateTime;
    Date mEndDateTime;

    /**
     * Interface to allow the passing of the events back to the host.
     */
    public interface HistoryDialogListener {
        public void onDialogHistorySet(DialogFragment dialog, Date start, Date end);
        public void onDialogCancel(DialogFragment dialog);
    }

    public HistoryDialog() {
        this(new Date(), new Date());
    }

    public HistoryDialog(Date start, Date end) {
        mStartDateTime = start;
        mEndDateTime = end;
    }

    public void setListener(HistoryDialogListener listener) {
        mListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
        View view =  inflater.inflate(R.layout.history_dialog, container, false);

        // Create DateTime Dialogs
        mStartDateTimeDialog = new DateTimeDialogFragment();
        mStartDateTimeDialog.setOnDateTimeChangedListener(new DateTimeDialogFragment.OnDateTimeChangedListener() {
            @Override
            public void onDateTimeChange(DateTimeDialogFragment dateTimeDialog, Date date) {
                mStartDateTime = date;
                mStartTimeButton.setText(dateTimeDialog.getDateTimeString());
            }
        });

        mStartTimeButton = (Button)view.findViewById(R.id.buttonStartHistory);
        mStartTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mStartDateTimeDialog.show(getFragmentManager(), "start");
                mStartDateTimeDialog.updateDateTime(mStartDateTime);
            }
        });
        mStartDateTimeDialog.updateDateTime(mStartDateTime);
        mStartTimeButton.setText(mStartDateTimeDialog.getDateTimeString());

        mEndDateTimeDialog = new DateTimeDialogFragment();
        mEndDateTimeDialog.setOnDateTimeChangedListener(new DateTimeDialogFragment.OnDateTimeChangedListener() {
            @Override
            public void onDateTimeChange(DateTimeDialogFragment dateTimeDialog, Date date) {
                mEndDateTime = date;
                mEndTimeButton.setText(dateTimeDialog.getDateTimeString());
            }
        });

        mEndTimeButton = (Button)view.findViewById(R.id.buttonEndHistory);
        mEndTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEndDateTimeDialog.show(getFragmentManager(), "end");
                mEndDateTimeDialog.updateDateTime(mEndDateTime);
            }
        });
        mEndDateTimeDialog.updateDateTime(mEndDateTime);
        mEndTimeButton.setText(mEndDateTimeDialog.getDateTimeString());

        mCancelButton = (Button)view.findViewById(R.id.buttonCancel);
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onDialogCancel(HistoryDialog.this);
            }
        });

        mSetButton = (Button)view.findViewById(R.id.buttonSet);
        mSetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onDialogHistorySet(HistoryDialog.this, mStartDateTime, mEndDateTime);
            }
        });
        return view;
    }

    /** The system calls this only when creating the layout in a dialog. */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){

        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

}
