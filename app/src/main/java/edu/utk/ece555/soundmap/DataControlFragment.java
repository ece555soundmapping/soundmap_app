package edu.utk.ece555.soundmap;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by ayoun on 11/14/2015.
 */
public class DataControlFragment extends Fragment {
    OnHistoryClickListener mHistoryCallback;
    OnLiveClickListener mLiveCallback;
    Button mLiveButton;
    Boolean mVisible = false;

    public interface OnHistoryClickListener {
        public void onHistoryClick();
    }

    public interface OnLiveClickListener {
        public void onLiveClick();
    }

    public void setListener(Object listener)
    {
        if (listener instanceof OnHistoryClickListener)
            mHistoryCallback = (OnHistoryClickListener)listener;

        if (listener instanceof OnLiveClickListener)
            mLiveCallback = (OnLiveClickListener)listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.data_control, container, false);

        Button historyButton = (Button)view.findViewById(R.id.buttonHistory);
        historyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mHistoryCallback != null)
                    mHistoryCallback.onHistoryClick();
            }
        });

        mLiveButton = (Button)view.findViewById(R.id.buttonLive);
        mLiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLiveButton != null)
                    mLiveCallback.onLiveClick();
            }
        });
        if (mVisible)
            mLiveButton.setVisibility(View.VISIBLE);
        else
            mLiveButton.setVisibility(View.GONE);;
        mLiveButton.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);

        return view;
    }

    public void setLiveVisibility(boolean visible) {
        mVisible = visible;

        if (mLiveButton != null) {
            if (visible)
                mLiveButton.setVisibility(View.VISIBLE);
            else
                mLiveButton.setVisibility(View.GONE);
        }
    }
}
