package edu.utk.ece555.soundmap;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;

import java.util.logging.LogRecord;

/**
 * Created by ayoun on 11/14/2015.
 */
public class PlaybackControlFragment extends Fragment implements Runnable {
    private PlaybackListener mListener = null;

    private Button mLiveButton;
    private Button mPlayButton;
    private SeekBar mSeekBar;
    private boolean mPlay = false;
    private int mPlayStep = 0;
    private Handler mPlayHandler = new Handler();
    private final int PLAY_DELAY = 18;
    private final int PLAY_TIME = 10000;

    public interface PlaybackListener {
        public void onLiveClick();
        public void onSeekChange(int seekValue);
    }

    public void setListener(Object listener) {
        if (listener instanceof PlaybackListener)
            mListener = (PlaybackListener) listener;
    }

    public void setPlaybackLength(int length) {
        mSeekBar.setMax(length);
    }

    public void stop(){
        try {
            mPlayButton.setText(getString(R.string.play_button));
            mPlay = false;
        } catch (Exception e) {}
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.playback_control, container, false);

        mLiveButton = (Button)view.findViewById(R.id.buttonLive);
        mPlayButton = (Button)view.findViewById(R.id.playButton);
        mSeekBar = (SeekBar)view.findViewById(R.id.seekBar);

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mListener != null)
                    mListener.onSeekChange(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mLiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.onLiveClick();
            }
        });
        mLiveButton.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);

        mPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mPlay) {
                    mPlayStep = (int)(mSeekBar.getMax()*((double)PLAY_DELAY)/PLAY_TIME);
                    mPlayButton.setText(getString(R.string.stop_button));
                    mPlay = true;
                    // Reset the progress if at the end.
                    if (mSeekBar.getProgress() == mSeekBar.getMax())
                        mSeekBar.setProgress(0);
                    mPlayHandler.postDelayed(PlaybackControlFragment.this, PLAY_DELAY);
                }
                else {
                    stop();
                }
            }
        });

        return view;
    }

    @Override
    public void run() {
        if (mPlay) {
            int value = mSeekBar.getProgress() + mPlayStep;

            if (value <= mSeekBar.getMax()) {
                mSeekBar.setProgress(value);
                mPlayHandler.postDelayed(this, PLAY_DELAY);
            } else {
                mSeekBar.setProgress(mSeekBar.getMax());
                stop();
            }
        }
    }
}
