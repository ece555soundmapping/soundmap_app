package edu.utk.ece555.soundmap;

import android.app.Activity;

import java.util.ArrayList;

/**
 * Created by ayoun on 11/14/2015.
 */
public class SensorNode {
    protected int mNodeId;
    protected ArrayList<SensorData> mData;

    public SensorNode(int id)
    {
        mNodeId = id;
        mData = new ArrayList<SensorData>();
    }

    public int getNodeId() {
        return mNodeId;
    }

    public ArrayList<SensorData> getData() {
        return mData;
    }
}
