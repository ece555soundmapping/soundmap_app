package edu.utk.ece555.soundmap;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.jjoe64.graphview.series.DataPoint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Owner on 11/14/2015.
 */
public class ServerListenForNew implements Runnable {
    private static final String ADDRESS = "50.142.48.24";
    private static final int PORT = 5556;
    private static final int MINUTES_IN_RANGE = 30;

    private Thread thread;
    private Socket socket;
    private BufferedReader data_read;
    private PrintWriter data_write;
    private ListenForNewListener mListener;
    private ArrayList<SensorNode> recentPoints;
    private Context mContext;

    private SimpleDateFormat ft;

    public interface ListenForNewListener {
        public void onNewSensorData(int id, SensorData value);
        public ArrayList<SensorNode> getCurrentSensorData();
    }

    public ServerListenForNew(Context context, ListenForNewListener listener) {
        mContext = context;
        this.mListener = listener;
        recentPoints = listener.getCurrentSensorData();
    }

    private boolean isNetworkAvailable() {
        Context context;
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void run() {
        String line;
        while(true) {
            while(!isNetworkAvailable()) {
            continue;
            }
            try {
                socket = new Socket(ADDRESS, PORT);
                data_read = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                data_write = new PrintWriter(socket.getOutputStream(), true);
            } catch (IOException e) {
                Log.e("serverListenForNew", "Failed to connect to server.", e);
                continue;
            }
            if(data_write == null){
                Log.e("serverListenForNew","data write is null");
                continue;
            }
            data_write.println("use Sound");
            try {
                String returnV = data_read.readLine();
                Log.i("serverListenForNew", "Return: " + returnV);
                if (!returnV.equals("F")) {
                    Log.e("serverListenForNow", "Failed set database.");
                    continue;
                }
            } catch (IOException e) {
                Log.e("serverListenForNew", "Failed read from server.", e);
                continue;
            }

            data_write.println("SUBSCRIBE");
            try {
                //Initialize table with results of Subscription
                String returnV = data_read.readLine();
                Log.i("serverListenForNew", "Return: " + returnV);
                if (!returnV.equals("R")) {
                    Log.e("serverListenForNow", "Failed subscribed database.");
                    continue;
                }
            } catch (IOException e) {
                Log.e("serverListenForNew", "Failed read from server.", e);
                continue;
            }

            //Clear Values in the Table
            recentPoints.clear();
            SensorNode newSensorNode;
            ArrayList<SensorData> newSensorDataList = null;
            int nodeNumber = -1;
            try {
                //Read First Data Point
                line = data_read.readLine();

                //Read Each Line
                while (!line.equals("F") && !line.equals("E")) {
                    //Get Array of Fields
                    String[] values = line.split("[,]");

                    //If sound level is not null, add value
                    Log.d("ListenForNow", line);
                    if (!values[2].equals("NULL")) {
                        //Make new structure if nodeNumber is new
                        if (Integer.parseInt(values[0]) != nodeNumber) {
                            nodeNumber = Integer.parseInt(values[0]);
                            newSensorNode = new SensorNode(nodeNumber);
                            recentPoints.add(newSensorNode);
                            newSensorDataList = newSensorNode.getData();

                            Date date = ft.parse(values[1]);
                            float slevel = Float.parseFloat(values[2]);
                            newSensorDataList.add(new SensorData(date, slevel));
                        }
                        //Add to existing structure if nodeNumber has been encountered
                        else if (newSensorDataList != null) {
                            Date date = ft.parse(values[1]);
                            float slevel = Float.parseFloat(values[2]);
                            newSensorDataList.add(new SensorData(date, slevel));
                        }
                    }
                    line = data_read.readLine();

                }
            } catch (Exception e) {
                Log.e("serverListenForNew", "Failed to read initilization points.", e);
                continue;
            }

            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(new Runnable() {
                @Override
                public void run() {
                    mListener.onNewSensorData(ServerService.ALL_SENSORS, null);
                }
            });

            Listen();
        }
    }

    public void Listen()
    {
        String line;
        SensorNode newSensorNode;
        ArrayList<SensorData> newSensorDataList = null;
        Date RecentDate;
        int  recentPointsIndex=-1;

        while(true){
            final int nodeId;
            final float slevel;
            final SensorData sensorData;
            try {
                recentPointsIndex = -1;
                //read until a SUB is encountered
                line = data_read.readLine();
                while (!line.equals("SUB")) {
                    line = data_read.readLine();
                }
                Log.d("serverListenForNew","Processed Sub");

                line = data_read.readLine();
                Log.d("serverListenForNew","Read"+line);
                //Get Array of Fields
                String[] values = line.split("[,]");
                Log.d("serverListenForNew","Preparing to Add Value " + values[2] + " for node " + values[0] + " as date " + values[1]);
                if (!values[2].equals("NULL")) {
                    Log.d("serverListenForNew","about to record node id");
                    nodeId = Integer.parseInt(values[0]);
                    Log.d("serverListenForNew","Preparing to Add Value for node "+Integer.toString(nodeId));
                    //Search for index in List
                    for (int i = 0; i < recentPoints.size(); i++) {
                        if (recentPoints.get(i).mNodeId == nodeId) {
                            recentPointsIndex = i;
                        }
                    }

                    //if node has values already in list, add the new one
                    if (recentPointsIndex != -1) {
                        ArrayList<SensorData> list = recentPoints.get(recentPointsIndex).getData();
                        RecentDate = ft.parse(values[1]);
                        slevel = Float.parseFloat(values[2]);
                        Log.d("serverListenForNew","Trying to add value for new Sensor");
                        sensorData = new SensorData(RecentDate, slevel);
                        list.add(sensorData);
                    } else {
                        newSensorNode = new SensorNode(nodeId);
                        recentPoints.add(newSensorNode);
                        newSensorDataList = newSensorNode.getData();

                        RecentDate = ft.parse(values[1]);
                        slevel = Float.parseFloat(values[2]);
                        Log.d("serverListenForNew","Trying to add value for existing Sensor");
                        sensorData = new SensorData(RecentDate, slevel);
                        newSensorDataList.add(sensorData);
                    }
                    Log.d("serverListenForNew","Getting value to filter");
                    //get filter time
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(RecentDate);
                    cal.add(Calendar.MINUTE, -1 * MINUTES_IN_RANGE);
                    Date filterDate = cal.getTime();

                    Log.d("serverListenForNew","About to filter recent points");
                    //filter recent Points to remove out-of-range values
                    for(SensorNode node : recentPoints){
                        ArrayList<SensorData> nodeData = node.getData();
                        while(!nodeData.isEmpty() && nodeData.get(0).mDate.before(filterDate)){
                            nodeData.remove(0);
                        }
                    }

                    Log.d("serverListenForNew","About to Run callback");
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            mListener.onNewSensorData(nodeId,sensorData);
                        }
                    });


                }
            }catch (Exception e){
                Log.e("serverListenForNew","Failed to read subscribe point", e);
                break;
            }

        }
    }

    public void start() {
        thread = new Thread(this, "serverListenForNew");
        thread.start();

        ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    }


}
